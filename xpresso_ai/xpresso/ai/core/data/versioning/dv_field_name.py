from enum import Enum


class DataVersioningFieldName(Enum):
    """
    Enum Class to fetch the constant strings used in Data Versioning module
    """
    REPO_NAME = "repo_name"
    BRANCH_NAME = "branch_name"
    COMMIT_ID = "commit_id"
    DATASET_NAME = "dataset_name"
    DESCRIPTION = "description"
    INPUT_PATH = "path"
    DATASET = "dataset"
    LIST = "list"
    PULL = "pull"
    DATA_TYPE = "data_type"
    DATA_TYPE_IS_FILE = "files"
    # Internal Code specific variables
    PUSH_FILES_MANDATORY_FIELDS = ["repo_name", "branch_name", "data_type",
                                   "dataset_name", "path", "descriptionLA"]
    PUSH_DATASET_MANDATORY_FIELDS = ["repo_name", "branch_name", "dataset",
                                     "description"]
    # output variables
    REPO_OUTPUT_NAME = "repo_name"
    FILE_TYPE = "type"
    OUTPUT_COMMIT_FIELD = "commit"
    OUTPUT_COMMIT_ID = "id"
    OUTPUT_BRANCH_HEAD = "head"
    PULL_OUTPUT_TYPE = "output_type"
    OUTPUT_FILE_TYPE = "files"
    PROJECTS = "projects"
    SUPER_USER = "su"
    BRANCH_TYPE = "type"
    BRANCH_TYPE_DATA = "data"
    BRANCH_TYPE_MODEL = "model"
    BRANCH_CREATED_BY = "created_by"
    BRANCH_CREATED_ON = "created_on"
    LAST_COMMIT_ID = "last_commit_id"
    LAST_COMMIT_BY = "last_commit_by"
    LAST_COMMIT_ON = "last_commit_on"
    BRANCH_COMMITS_KEY = "commits"
    SIZE_OF_BRANCH = "total_file_size"
    TOTAL_FIlES_IN_BRANCH = "number_of_files"
    XPRESSO_COMMIT_ID = "xpresso_commit_id"
    DV_COMMIT_ID = "dv_commit_id"
    COMMITTED_BY = "committed_by"
    COMMITTED_ON = "committed_on"
    TOTAL_FILES_IN_COMMIT = "number_of_files"
    SIZE_OF_COMMIT = "total_file_size"
    BRANCH_COLLECTION = "branches"
    DV_PROJECT_TOKEN_USER = "project_token_user"

    def __str__(self):
        """
        Overriding builtin python's string representation of object
        """
        return self.value

